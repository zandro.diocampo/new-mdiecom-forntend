
import {Row, Col, Card} from 'react-bootstrap';
export default function Highlights(){
	return(

		<Row className='mt-3 mb-3'>
			<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
			<div id="toysImg"></div>
				<Card.Body>
					<Card.Title>
					<h2>Toys</h2>
					</Card.Title>
					<Card.Text>

					For dogs and other pets, toys are not a luxury, but a necessity. 
					Toys are important to your dog's well-being. Toys help fight boredom 
					when you have to leave your dog at home, and provide comfort when 
					they're feeling nervous. Toys can even help prevent your dog from 
					developing certain problem behaviors.
					</Card.Text>
				</Card.Body>
			</Card>
			</Col>

			<Col xs={12} md={4}>
			<Card className="cardHighlight p-3" id="treatsImg">
				<Card.Body>
					<Card.Title>
					<h2>Treats</h2>
					</Card.Title>
					<Card.Text>
					Anyone with a dog knows the importance of treats. 
					Not only are they an invaluable training tool, they also play a 
					huge role in forging a bond between you and your pet. Giving your 
					dog a well-earned treat is a display of affection that they really 
					appreciate.					
					</Card.Text>
				</Card.Body>
			</Card>
			</Col>

			<Col xs={12} md={4}>
			<Card className="cardHighlight p-3" id="gearsImg">
				<Card.Body>
					<Card.Title>
					<h2>Essential Gears</h2>
					</Card.Title>
					<Card.Text>
					Going out for a nice brisk walk with your dog can be a pleasure 
					for both pooch and person. But it can also turn into a bit of a 
					nightmare for both if you aren’t using the right dog supplies- 
					such as a collar, lead, or harness.
					</Card.Text>
				</Card.Body>
			</Card>
			</Col>
		</Row>

	)
}