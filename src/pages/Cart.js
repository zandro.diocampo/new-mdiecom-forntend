import React, { useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import CartContext from '../CartContext';

export default function Cart() {
  const { cartItems, removeFromCart } = useContext(CartContext);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          {cartItems.length === 0 ? (
            <h3>Your cart is empty.</h3>
          ) : (
            cartItems.map((item) => (
              <Card key={item.id} className="mb-3">
                <Card.Body>
                  <Card.Title>{item.name}</Card.Title>
                  <Card.Text>{item.description}</Card.Text>
                  <p>Quantity: {item.quantity}</p>
                  <Card.Subtitle>Price: {item.price}</Card.Subtitle>
                  <Button
                    variant="danger"
                    onClick={() => removeFromCart(item.id)}
                  >
                    Remove
                  </Button>
                </Card.Body>
              </Card>
            ))
          )}
        </Col>
      </Row>
    </Container>
  );
}
