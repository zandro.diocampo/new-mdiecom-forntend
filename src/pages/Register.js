import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Form, Button, Card} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	function registerUser(e){
		 e.preventDefault();

		 fetch('https://mdi-ecom-backend.onrender.com/users/checkEmailExists',{
		 	method: 'POST',
		 	headers: {
		 		'Content-Type' : 'application/json'
		 	},
		 	body: JSON.stringify({
		 		email:email
		 	})
		 })
		 .then(res => res.json())
		 .then(data => {
		 	console.log(data);

		 	if(data){
		 		Swal.fire({
		 			title: "Duplicate email found",
		 			icon: "info",
		 			text: "The email already exists"
		 		});
		 	} else {
		 		fetch('https://mdi-ecom-backend.onrender.com/users/register',{
		 			method: 'POST',
		 			headers: {
		 				'Content-Type' : 'application/json'
		 			},
		 			body: JSON.stringify({
		 				email    : email,
		 				password : password,
		 				firstName: firstName,
		 				lastName : lastName,
		 				mobileNo : mobileNo
		 			})
		 		})
		 		.then(res => res.json())
		 		.then(data =>{
		 			console.log(data);

		 			if(data.email){
		 				Swal.fire({
		 					title: "Registration Successful",
		 					icon: "success",
		 					text: "Thank you for registering"
		 				});
		 				history("/login");
		 			} else {
		 				Swal.fire({
		 					title: "Registration Failed",
		 					icon: "error",
		 					text: "Something went wrong, try again"
		 				})
		 			}
		 		})
		 	}
		 })

		 // clear input fields
		 setEmail('');
		 setPassword('');
		 setFirstName('');
		 setLastName('');
		 setMobileNo('');

		 // alert('Thank you for registering');
	};


	// Syntax:
		// useEffect(() => {}, [])	
	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && password !== '' && firstName !=='' && lastName !== '' && mobileNo !== '' && mobileNo.length ===11 ){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password, firstName, lastName, mobileNo]);

	return(
		
		(user.id !== null)?
			<Navigate to="/Product"/>
		:
		<>
		<h1 id="titleRegister">Register Here:</h1>
	<div id="regImg">
	<Card className="p-3 w-50" id="registerForm">
		<Form onSubmit={e => registerUser(e)}>
		<Form.Group controlId="firstName" className="mb-3">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="First Name" className="mb-3"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName" className="mb-3">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo" className="mb-3">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Mobile Number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

				<Form.Group controlId="userEmail" className="mb-3">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						required
						value= {email}
						onChange= {e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
				</Form.Group>

				

		{ isActive ?			
			<Button className="mt-3 mb-2" variant="success" type="submit" id="submitBtn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-2" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}
		<p>Already have an account? <Link to="/login">Login here</Link></p>

		</Form>
	</Card>
	</div>
		</>

		


	)
}


