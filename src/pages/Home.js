import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import '../App.css';
export default function Home(){

	return(
		<>	
		<div id="jumbotron">
			<Banner/>
		</div>
		<div id="btn" className="d-none d-xs-flex">
		<Button id="btn" variant="primary" as={Link} to="/Product">Shop now</Button>
		</div>
			<Highlights/>

		</>
	)
}

