import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import CartContext from '../CartContext';
// import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Profile(){

	const {user} = useContext(UserContext);
	const { getTotalItems } = useContext(CartContext);

	// allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a product
	const history = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");


	const {userId} = useParams();

	

	useEffect(() => {

		fetch('https://mdi-ecom-backend.onrender.com/users/details/:userId',{
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
	})


	}, [firstName, lastName, email,]);



	
	return (
		<Container className="mt-5">
			<Row>
				<Col>
					
					<Card className="p-2">
					      <Card.Body>
					      	<h3 className="pb-3">Profile:</h3>
					        <Card.Title className="mb-3">Name: {lastName}, {firstName}</Card.Title>
					        <Card.Subtitle className="mb-3">email: {email}</Card.Subtitle>
							<h5>Items in cart : {getTotalItems()}</h5>
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>



	)

}